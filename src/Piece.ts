export const enum PieceType {
    Pawn = "P",
    Knight = "N",
    Bishop = "B",
    Rook = "R",
    Queen = "Q",
    King = "K",
}

export class Piece {
    isWhite: boolean;
    pieceType: PieceType;
    hasMovedBefore: boolean = false;
    x: number;
    y: number;

    constructor(isWhite: boolean, pieceType: PieceType, x: number, y: number) {
        this.isWhite = isWhite;
        this.pieceType = pieceType;
        this.x = x;
        this.y = y;
    }
}
