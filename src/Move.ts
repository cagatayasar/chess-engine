import { Piece } from './Piece'

export const enum MoveType {
    Move,
    Take,
    MoveOrTake,
    // Castle,
}

export class Move {
    moveType: MoveType;
    piece: Piece;
    relativeX: number;
    relativeY: number;

    constructor(moveType: MoveType, piece: Piece, relativeX: number, relativeY: number) {
        this.moveType = moveType;
        this.piece = piece;
        this.relativeX = relativeX;
        this.relativeY = relativeY;
    }
}