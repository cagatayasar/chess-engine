import { Piece, PieceType } from "./Piece"
import { Move, MoveType } from "./Move"

const startingPieceTypesRow1: PieceType[] = [
    PieceType.Rook, PieceType.Knight, PieceType.Bishop, PieceType.Queen,
    PieceType.King, PieceType.Bishop, PieceType.Knight, PieceType.Rook
];
const startingPieceTypesRow2: PieceType[] = new Array(8).fill(PieceType.Pawn);

export function CreateDefaultBoard(): Piece[][] {
    const board: Piece[][] = new Array(8)
        .fill(null)
        .map(() => new Array(8).fill(null));

    board[0] = startingPieceTypesRow1.map((p, i) => new Piece(true,  p, i, 0));
    board[1] = startingPieceTypesRow2.map((p, i) => new Piece(true,  p, i, 1));
    board[6] = startingPieceTypesRow2.map((p, i) => new Piece(false, p, i, 6));
    board[7] = startingPieceTypesRow1.map((p, i) => new Piece(false, p, i, 7));

    return board;
}

export function PrintBoard(board: Piece[][]): void {
    console.log("BOARD START\n--------")
    board.slice().reverse().forEach(row => {
        const rowString = row
            .map((p): string => {
                if (p == null) return "_";
                if (p.isWhite) return p.pieceType;
                return p.pieceType.toLowerCase();
            })
            .reduce((p1, p2) => p1 + p2);
        console.log(rowString);
    });
    console.log("--------\nBOARD START END");
}

export function GetPossibleMoves(board: Piece[][], piece: Piece): Move[] {
    const moves: Move[] = [];

    if (piece.pieceType == PieceType.Pawn) {
        const yDirection = piece.isWhite ? 1 : -1;
        if (!piece.hasMovedBefore) {
            moves.push(new Move(MoveType.Move, piece, 0, 2 * yDirection));
        }
        moves.push(new Move(MoveType.Move, piece,  0, 1 * yDirection));
        moves.push(new Move(MoveType.Take, piece, -1, 1 * yDirection));
        moves.push(new Move(MoveType.Take, piece,  1, 1 * yDirection));
    } else if (piece.pieceType == PieceType.Knight) {
        moves.push(
            new Move(MoveType.MoveOrTake, piece,  1,  2),
            new Move(MoveType.MoveOrTake, piece,  1, -2),
            new Move(MoveType.MoveOrTake, piece, -1,  2),
            new Move(MoveType.MoveOrTake, piece, -1, -2),
            new Move(MoveType.MoveOrTake, piece,  2,  1),
            new Move(MoveType.MoveOrTake, piece,  2, -1),
            new Move(MoveType.MoveOrTake, piece, -2,  1),
            new Move(MoveType.MoveOrTake, piece, -2, -1),
        );
    } else if (piece.pieceType == PieceType.King) {
        [0, 1, -1].forEach(x => [0, 1, -1].forEach(y =>
            moves.push(new Move(MoveType.MoveOrTake, piece, x, y))
        ));
    }

    if (piece.pieceType == PieceType.Bishop || piece.pieceType == PieceType.Queen) {
        moves.push(
            ...GetDirectionalMoves(board, piece,  1,  1),
            ...GetDirectionalMoves(board, piece,  1, -1),
            ...GetDirectionalMoves(board, piece, -1,  1),
            ...GetDirectionalMoves(board, piece, -1, -1),
        );
    }
    if (piece.pieceType == PieceType.Rook || piece.pieceType == PieceType.Queen) {
        moves.push(
            ...GetDirectionalMoves(board, piece,  1,  0),
            ...GetDirectionalMoves(board, piece, -1,  0),
            ...GetDirectionalMoves(board, piece,  0,  1),
            ...GetDirectionalMoves(board, piece,  0, -1),
        );
    }

    return moves.filter(move => IsMoveValid(board, move));
}

function GetDirectionalMoves(board: Piece[][], piece: Piece, xDirection: 0 | 1 | -1, yDirection: 0 | 1 | -1): Move[] {
    const moves: Move[] = [];
    let nextX = piece.x + xDirection;
    let nextY = piece.y + yDirection;
    while (nextX >= 0 && nextX <= 7 && nextY >= 0 && nextY <= 7) {
        const pieceAtPos = board[nextY][nextX];
        if (pieceAtPos == null) {
            moves.push(new Move(MoveType.MoveOrTake, piece, nextX - piece.x, nextY - piece.y));
        } else if (pieceAtPos.isWhite != piece.isWhite) {
            moves.push(new Move(MoveType.MoveOrTake, piece, nextX - piece.x, nextY - piece.y));
            break;
        }
    }
    return moves;
}

function IsMoveValid(board: Piece[][], move: Move): boolean {
    const x = move.piece.x + move.relativeX;
    const y = move.piece.y + move.relativeY;
    if (x < 0 || x > 7 || y < 0 || y > 7) {
        return false;
    }

    const pieceAtPos = board[y][x];
    if (pieceAtPos == null) {
        return move.moveType == MoveType.Move || move.moveType == MoveType.MoveOrTake;
    }

    return move.moveType == MoveType.Take || move.moveType == MoveType.MoveOrTake;
}
